<?php

  

namespace App\Http\Controllers;

  

use App\Product;

use Illuminate\Http\Request;

use Image;
  

class ProductController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        $products = Product::latest()->paginate(5);

  

        return view('products.index',compact('products'))

            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

   

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('products.create');

    }

  

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

         $this->validate($request, array(

            'name' => 'required',

            'detail' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ));

       /* $imageName = time().'.'.request()->image->getClientOriginalExtension();


  

        $imageName = time().'.'.request()->image->getClientOriginalExtension();

  

        $request->image->move(public_path('images'), $imageName);
  

        Product::create($request->all());
*/
     $person  = new product ;
        $person->name = $request->name;
        $person->detail = $request->detail;

        if($request->hasFile('image')){
          $imageName = time().'.'.request()->image->getClientOriginalExtension();


  

        $imageName = time().'.'.request()->image->getClientOriginalExtension();

  

        $request->image->move(public_path('images'), $imageName);
          $person->image = $imageName;
          $person->save();
        };

      $person->save();


        return redirect()->route('products.index')

                        ->with('success','Product created successfully.');

    }

   

    /**

     * Display the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function show(Product $product)

    {

        return view('products.show',compact('product'));

    }

   

    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function edit(Product $product)

    {

        return view('products.edit',compact('product'));

    }

  

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Product $product)

    {

        $request->validate([

            'name' => 'required',

            'detail' => 'required',

        ]);

  

        $product->update($request->all());

  

        return redirect()->route('products.index')

                        ->with('success','Product updated successfully');

    }

  

    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function destroy(Product $product)

    {

        $product->delete();

  

        return redirect()->route('products.index')

                        ->with('success','Product deleted successfully');

    }

}